# Prerequisites

1. The first prerequisite is to have Python 🐍 with the Pip package manager installed. If you don't have Python and Pip already, an easy solution is to get them from the Anaconda or the Miniconda distributions:
   - [The Anaconda distribution](https://www.anaconda.com/products/individual), which is a huge full featured distribution with many things included, like a graphical interface.
   - [The Miniconda distribution](https://docs.conda.io/en/latest/miniconda.html), which is a leaner distribution, and is command line only.

Miniconda is good if you know your way around the command line. Otherwise I would suggest to choose the Anaconda distribution.

2. The second prerequisite is to have Git installed. In case you don't have it already, you can download it from [here](https://git-scm.com/).

3. You'll also need to create a project on Gitlab. Please ask if you don't know where to create you project. ⚠️ When you create your project on Gitlab, the checkbox ☑️ _Initialize repository with a README_ should be **unchecked**. Once your project has been created, you'll need to get the URL of your project, which can be found under the _Clone_ button. It should be something like this: `https://gitlab.com/fleischmann-lab/copier-template.git`. ⚠️ Note the `.git` at the end!

# How to generate your project from this template

First install [Copier](https://github.com/copier-org/copier) with:

```
conda create --name copier python=3.10
conda activate copier
conda install -c conda-forge copier
```

You can check that `Copier` has been correctly installed by running the following command, which should output a version number:

```
copier --version
```

Then you can just run the following command and replace the string `path/to/directory` by the path to the directory where you want to create your new project.

```
copier https://gitlab.com/fleischmann-lab/copier-template.git "path/to/directory"
```

Answer the questions, and when it's finished you should get a project structure 🌳 like the following:

```
.
├── LICENSE
├── README.md
├── environment.yml
├── .gitlab-ci.yml
├── .flake8
├── .gitignore
├── config/
├── data/
├── docs/
├── notebooks/
├── src/
└── tests/
```

Also your new project should be linked with your online Git repository. You can check this by running `git remote -v` from your newly created project, which should output something like this:

```
$ cd "path/to/directory"
$ git remote -v
origin  https://gitlab.com/fleischmann-lab/copier-template.git (fetch)
origin  https://gitlab.com/fleischmann-lab/copier-template.git (push)
```

# Next steps (recommended)

1. Now you can run the following commands to push the files on your local machine to the server:
   1. `git add -A`
   2. `git commit -m "My awesome first commit"`
   3. `git push -u origin master` (Do let me know in case your push get rejected for some reason, depending on where you push, there may be some write permissions to change)
2. Then you should follow the instructions in the newly created `README.md` file to setup your Conda environment with JupyterLab 🚀

# Troubleshooting ⚠️

## macOS

- If you get the following error: `ValueError: unknown locale: UTF-8`, you can paste the following lines in your terminal to fix it:

```
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
```

- If you get the following error `xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun`, you can solve it by running this in your terminal:

```
xcode-select --install
```
