from pathlib import Path
import shutil
import subprocess
import os

from copier import run_copy
import yaml

ROOT_LOC = Path(__file__).resolve().parent
TEMPLATE_LOC = ROOT_LOC.joinpath("template-test")
COPIER_FILE = ROOT_LOC.joinpath("copier.yml")
GIT_REMOTE_URL = "http://blablabla.git"


def get_yaml_dict(yaml_file):
    with open(yaml_file) as fid:
        copier_vars = yaml.safe_load(fid)
    return copier_vars


class GenerateTemplate:
    def __enter__(self):
        copier_vars = get_yaml_dict(COPIER_FILE)
        template_vars = {
            "environment_name": copier_vars.get("environment_name").get("default"),
            "git_remote_url": GIT_REMOTE_URL,
        }

        # Generate a template
        run_copy(
            src_path=str(ROOT_LOC),
            dst_path=str(TEMPLATE_LOC),
            data=template_vars,
            cleanup_on_error=True,
        )

    def __exit__(self, exc_type, exc_value, traceback):
        # Remove the template
        shutil.rmtree(TEMPLATE_LOC)


def test_template_copy():
    with GenerateTemplate():
        path_list_to_check = [
            TEMPLATE_LOC.joinpath("src"),
            TEMPLATE_LOC.joinpath("notebooks"),
            TEMPLATE_LOC.joinpath(".gitlab-ci.yml"),
            TEMPLATE_LOC.joinpath("LICENSE"),
            TEMPLATE_LOC.joinpath("README.md"),
            TEMPLATE_LOC.joinpath("environment.yml"),
            TEMPLATE_LOC.joinpath(".git"),
        ]
        for item in path_list_to_check:
            assert Path.exists(item)


def test_Readme_templating():
    with GenerateTemplate():
        Readme_loc = TEMPLATE_LOC.joinpath("README.md")
        with open(Readme_loc, "r") as fid:
            file_content = fid.read()
        copier_vars = get_yaml_dict(COPIER_FILE)
        env_name = copier_vars.get("environment_name").get("default")
        assert f"conda activate {env_name}" in file_content
        assert f"python -m ipykernel install --user --name={env_name}" in file_content
        assert (
            f"Once in JupyterLab, don't forget to change your kernel to {env_name}"
            in file_content
        )


def test_Environment_templating():
    with GenerateTemplate():
        environment_loc = TEMPLATE_LOC.joinpath("environment.yml")
        env_vars = get_yaml_dict(environment_loc)
        copier_vars = get_yaml_dict(COPIER_FILE)
        env_name = copier_vars.get("environment_name").get("default")
        assert env_vars.get("name") == str(env_name)


def get_git_remote():
    os.chdir(TEMPLATE_LOC)
    args = ["git", "remote", "-v"]
    res = subprocess.run(args, stdout=subprocess.PIPE)
    output = res.stdout.decode()
    return output


def test_git_remote_task():
    with GenerateTemplate():
        git_remote_out = get_git_remote()
        assert GIT_REMOTE_URL in git_remote_out
